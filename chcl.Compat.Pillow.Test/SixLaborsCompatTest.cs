using System.Reflection;
using SixLabors.ImageSharp.Processing.Processors.Filters;
using SixLabors.ImageSharp.Processing.Processors.Transforms;

namespace chcl.Compat.Pillow.Test;

[TestClass]
public class SixLaborsCompatTest
{
    Image<Rgb24> _baseImage = Image.Load<Rgb24>(GetSampleImageStream("base.png"));
    
    private static Stream GetSampleImageStream(string name)
    {
        return Assembly.GetExecutingAssembly().GetManifestResourceStream($"chcl.Compat.Pillow.Test.Samples.{name}") ??
               throw new ArgumentException($"File '{name}' not found in samples!", nameof(name));
    }

    private static void CompareImages<TPixel>(Image<TPixel> expected, Image<TPixel> actual, Image<Rgb24> source)
        where TPixel : unmanaged, IPixel<TPixel>
    {
        actual.ProcessPixelRows(expected, source, (mutatedAccessor, truthAccessor, baseAccessor) =>
        {
            Assert.AreEqual(truthAccessor.Width, mutatedAccessor.Width);
            Assert.AreEqual(truthAccessor.Height, mutatedAccessor.Height);

            for (var i = 0; i < mutatedAccessor.Height; i++)
            {
                var mutatedSpan = mutatedAccessor.GetRowSpan(i);
                var truthSpan = truthAccessor.GetRowSpan(i);
                var baseSpan = baseAccessor.GetRowSpan(i);
                
                for (var j = 0; j < mutatedAccessor.Width; j++)
                {
                    Assert.AreEqual(truthSpan[j], mutatedSpan[j], $"Pixel [{j+1},{i+1}] does not match! Original is [{baseSpan[j].R}, {baseSpan[j].G}, {baseSpan[j].B}]");
                }
            }
        });
    }
    
    private static void CompareImages<TPixel>(Image<TPixel> expected, Image<TPixel> actual)
        where TPixel : unmanaged, IPixel<TPixel>
    {
        actual.ProcessPixelRows(expected, (mutatedAccessor, truthAccessor) =>
        {
            Assert.AreEqual(truthAccessor.Width, mutatedAccessor.Width);
            Assert.AreEqual(truthAccessor.Height, mutatedAccessor.Height);

            for (var i = 0; i < mutatedAccessor.Height; i++)
            {
                var mutatedSpan = mutatedAccessor.GetRowSpan(i);
                var truthSpan = truthAccessor.GetRowSpan(i);
                
                for (var j = 0; j < mutatedAccessor.Width; j++)
                {
                    Assert.AreEqual(truthSpan[j], mutatedSpan[j], $"Pixel [{j+1},{i+1}] does not match!");
                }
            }
        });
    }

    [TestMethod]
    public void Bt601GrayscalingIdentical()
    {
        var baseImage = _baseImage.CloneAs<Rgb24>();
        var targetImage = Image.Load<L8>(GetSampleImageStream("base_L.png"));
        
        baseImage.Mutate(x => x.ApplyProcessor(new PillowGreyscale()));
        var mutatedImage = baseImage.CloneAs<L8>();
        
        CompareImages(targetImage, mutatedImage, baseImage);
    }
    
    [TestMethod]
    public void BilinearScalingIdentical()
    {
        var baseImage = Image.Load<L8>(GetSampleImageStream("base_L.png"));
        var targetImage = Image.Load<L8>(GetSampleImageStream("base_LHalfBilinear.png"));
        
        baseImage.Mutate(x => x.Resize(2048, 2048, new TriangleResampler()));
        var mutatedImage = baseImage.CloneAs<L8>();
        
        CompareImages(targetImage, mutatedImage);
    }
}