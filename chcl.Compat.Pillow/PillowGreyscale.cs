﻿using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing.Processors;

namespace chcl.Compat.Pillow;

public class PillowGreyscale<TPixel> : ImageProcessor<TPixel> where TPixel : unmanaged, IPixel<TPixel>
{
    public PillowGreyscale(Configuration configuration, Image<TPixel> source, Rectangle sourceRectangle) : base(configuration, source, sourceRectangle)
    {
    }

    protected override void OnFrameApply(ImageFrame<TPixel> source)
    {
        var rgba32 = new Rgba32();
        for (int y = 0; y < source.PixelBuffer.Height; y++)
        {
            var span = source.PixelBuffer.DangerousGetRowSpan(y);
            for (int x = 0; x < source.PixelBuffer.Width; x++)
            {
                span[x].ToRgba32(ref rgba32);
                span[x].FromL8(
                    new L8((byte)((rgba32.R * 19595 + rgba32.G * 38470 + rgba32.B * 7471 + 0x8000) >> 16)));
            }
        }
    }
}

public class PillowGreyscale : IImageProcessor
{
    public IImageProcessor<TPixel> CreatePixelSpecificProcessor<TPixel>(Configuration configuration, Image<TPixel> source,
        Rectangle sourceRectangle) where TPixel : unmanaged, IPixel<TPixel>
    {
        return new PillowGreyscale<TPixel>(configuration, source, sourceRectangle);
    }
}