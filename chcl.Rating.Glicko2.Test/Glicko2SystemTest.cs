namespace chcl.Rating.Glicko2.Test;

[TestClass]
public class Glicko2SystemTest
{
    [TestMethod]
    public void PaperExampleMatches()
    {
        //Suppose a player rated 1500 competes against players rated 1400, 1550 and 1700, winning
        //the first game and losing the next two. Assume the 1500-rated player’s rating deviation
        //is 200, and his opponents’ are 30, 100 and 300, respectively. Assume the 1500 player has
        //volatility σ = 0.06, and the system constant τ is 0.5.
        
        var system = new Glicko2System(0.5);
        var player1 = new Player(1, 1500, 200, 0.06);
        var player2 = new Player(2, 1400, 30);
        var player3 = new Player(3, 1550, 100);
        var player4 = new Player(4, 1700, 300);
        system.TrackPlayer(player1);
        system.TrackPlayer(player2);
        system.TrackPlayer(player3);
        system.TrackPlayer(player4);
        
        system.RecordGame(player1, player2, GameOutcome.AWon);
        system.RecordGame(player1, player3, GameOutcome.BWon);
        system.RecordGame(player1, player4, GameOutcome.BWon);
        system.ConcludeRatingPeriod();

        player1 = system.Players[1];
        Assert.AreEqual(1464.06, player1.Rating, 0.01);
        Assert.AreEqual(151.52, player1.RatingDeviation, 0.01);
        Assert.AreEqual(0.05999, player1.RatingVolatility, 0.00001);
    }

    [TestMethod]
    public void InactivePlayerIsUpdated()
    {
        var system = new Glicko2System(0.5);
        var player1 = new Player(1);
        var player2 = new Player(2);
        var player3 = new Player(3);
        
        system.TrackPlayer(player1);
        system.TrackPlayer(player2);
        system.TrackPlayer(player3);
        system.RecordGame(player1, player2, GameOutcome.AWon);
        system.ConcludeRatingPeriod();
        
        // Note that if a player does not compete during the rating period, then only Step 6 applies. In
        // this case, the player’s rating and volatility parameters remain the same, but the RD increases
        // according to φ′ = φ∗ = sqrt(φ^2 + σ^2).
        Assert.AreEqual(player3.Rating, system.Players[3].Rating);
        Assert.AreEqual(player3.RatingVolatility, system.Players[3].RatingVolatility);
        Assert.AreEqual(Math.Sqrt(Math.Pow(player3.RatingDeviationRaw, 2) + Math.Pow(player3.RatingVolatility, 2)), system.Players[3].RatingDeviationRaw);
    }
}