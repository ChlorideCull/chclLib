﻿namespace chcl.Rating.Glicko2;

/// <summary>
/// Outcome of a zero-sum game.
/// </summary>
/// <seealso cref="Glicko2System.RecordGame(long,long,chcl.Rating.Glicko2.GameOutcome)"/>
public enum GameOutcome
{
    /// <summary>
    /// "A" won, "B" lost.
    /// </summary>
    AWon,
    /// <summary>
    /// "B" won, "A" lost.
    /// </summary>
    BWon,
    /// <summary>
    /// "A" and "B" drew.
    /// </summary>
    Draw
}