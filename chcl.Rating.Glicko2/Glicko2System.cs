﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;

namespace chcl.Rating.Glicko2;

/// <summary>
/// Represents a Glicko-2 rating system, where in players can be compared, and ratings can be updated.
/// </summary>
/// <seealso href="http://www.glicko.net/glicko/glicko2.pdf"/>
public class Glicko2System
{
    private readonly double _systemConstant;
    private Dictionary<long, Player> _trackedPlayers = new();
    private List<(long, long, GameOutcome)> _gamesInCurrentPeriod = new();

    /// <summary>
    /// Access a read-only list of tracked <see cref="Player"/>s, indexed by their IDs.
    /// </summary>
    public IReadOnlyDictionary<long, Player> Players => _trackedPlayers;

    /// <summary>
    /// Initialize a rating system.
    /// </summary>
    /// <param name="systemConstant">System constant, which constrains the change in volatility over time. The
    /// paper says "Reasonable choices are between 0.3 and 1.2, though the system should be tested to decide which
    /// value results in greatest predictive accuracy. Smaller values prevent the volatility measures from changing
    /// by large amounts, which in turn prevent enormous changes in ratings based on very improbable results. If the
    /// application of Glicko-2 is expected to involve extremely improbable collections of game outcomes, then it
    /// should be set to a small value, even as small as, say, 0.2."</param>
    public Glicko2System(double systemConstant = 0.3)
    {
        _systemConstant = systemConstant;
    }

    /// <summary>
    /// Add a player to be tracked in the system. They must be in the system to participate in games.
    /// </summary>
    /// <param name="player">Player who should be tracked in the system.</param>
    /// <exception cref="ArgumentException">Thrown if the player is already tracked.</exception>
    public void TrackPlayer(Player player)
    {
        if (!_trackedPlayers.TryAdd(player.Id, player))
        {
            throw new ArgumentException("Player is already tracked.", nameof(player));
        }
    }

    /// <summary>
    /// Record a game in the current rating period. A game should only be recorded *once*.
    /// </summary>
    /// <param name="a">Player A.</param>
    /// <param name="b">Player B.</param>
    /// <param name="outcome">Which of the players won, or if it was a draw.</param>
    /// <exception cref="ArgumentException">Thrown if a player is not tracked. They should be tracked
    /// with <see cref="TrackPlayer"/> before this function is called.</exception>
    public void RecordGame(long a, long b, GameOutcome outcome)
    {
        if (!_trackedPlayers.ContainsKey(a))
        {
            throw new ArgumentException("Player A is not tracked, have you called TrackPlayer?", nameof(a));
        }
        if (!_trackedPlayers.ContainsKey(b))
        {
            throw new ArgumentException("Player B is not tracked, have you called TrackPlayer?", nameof(b));
        }
        
        _gamesInCurrentPeriod.Add((a, b, outcome));
    }

    /// <inheritdoc cref="RecordGame(long,long,chcl.Rating.Glicko2.GameOutcome)"/>
    public void RecordGame(Player a, Player b, GameOutcome outcome) => RecordGame(a.Id, b.Id, outcome);

    /// <summary>
    /// Conclude a rating period, applying all pending recorded games, and updating all ratings, deviations,
    /// and volatility scores of all tracked players.
    /// </summary>
    /// <remarks>The paper says "The Glicko-2 system works best when the number of games in a rating period is moderate
    /// to large, say an average of at least 10-15 games per player in a rating period. The length of time for a
    /// rating period is at the discretion of the administrator."</remarks>
    public void ConcludeRatingPeriod()
    {
        // First, we "fill out" the list of games so it reflects both sides, and group them by player A's ID.
        var allGames = _gamesInCurrentPeriod
            .Concat(_gamesInCurrentPeriod.Select(x => (x.Item2, x.Item1, x.Item3 switch
                {
                    GameOutcome.AWon => GameOutcome.BWon,
                    GameOutcome.BWon => GameOutcome.AWon,
                    _ => GameOutcome.Draw
                })))
            .GroupBy(x => x.Item1)
            .ToDictionary(x => x.Key, x => x.ToArray());
        _gamesInCurrentPeriod.Clear(); // No longer required, since we've evaluated it up above.
        
        var newPlayers = _trackedPlayers.Values
            .Select(f => EvaluatePlayer(f, allGames.TryGetValue(f.Id, out var games) ? games : null))
            .ToDictionary(x => x.Id);
        _trackedPlayers = newPlayers;
    }

    [Pure]
    private Player EvaluatePlayer(Player player, (long, long, GameOutcome)[]? games)
    {
        // You'll want this to follow along, because math: http://www.glicko.net/glicko/glicko2.pdf
        
        var v = Math.Pow(games?.Select(x =>
        {
            var op = _trackedPlayers[x.Item2];
            return Math.Pow(GFunc(op.RatingDeviationRaw), 2) *
                   EFunc(player.RatingRaw, op.RatingRaw, op.RatingDeviationRaw) *
                   (1 - EFunc(player.RatingRaw, op.RatingRaw, op.RatingDeviationRaw));
        }).Sum() ?? double.NaN, -1);
        var quantity = v * games?.Select(x =>
        {
            var op = _trackedPlayers[x.Item2];
            var outcome = x.Item3 switch
            {
                GameOutcome.Draw => 0.5,
                GameOutcome.AWon => 1,
                GameOutcome.BWon => 0,
                _ => throw new ArgumentOutOfRangeException(nameof(x))
            };
            return GFunc(op.RatingDeviationRaw) *
                   (outcome -EFunc(player.RatingRaw, op.RatingRaw, op.RatingDeviationRaw));
        }).Sum() ?? double.NaN;

        var newVolatility = games == null ? player.RatingVolatility : CalculateNewVolatility(player.RatingVolatility, quantity, player.RatingDeviationRaw, v);

        var newPreRatingPeriodDeviation =
            Math.Sqrt(Math.Pow(player.RatingDeviationRaw, 2) + Math.Pow(newVolatility, 2));
        var newDeviation = games == null ? newPreRatingPeriodDeviation : 1 / Math.Sqrt((1 / Math.Pow(newPreRatingPeriodDeviation, 2)) + (1 / v));
        var newRating = player.RatingRaw + Math.Pow(newDeviation, 2) * games?.Select(x =>
        {
            var op = _trackedPlayers[x.Item2];
            var outcome = x.Item3 switch
            {
                GameOutcome.Draw => 0.5,
                GameOutcome.AWon => 1,
                GameOutcome.BWon => 0,
                _ => throw new ArgumentOutOfRangeException(nameof(x))
            };
            return GFunc(op.RatingDeviationRaw) *
                   (outcome - EFunc(player.RatingRaw, op.RatingRaw, op.RatingDeviationRaw));
        }).Sum() ?? double.NaN;

        var scaledRating = 173.7178 * newRating + 1500;
        var scaledRd = 173.7178 * newDeviation;

        return games == null ? 
            new Player(player.Id, player.Rating, scaledRd, player.RatingVolatility) :
            new Player(player.Id, scaledRating, scaledRd, newVolatility);
    }

    [Pure]
    private double CalculateNewVolatility(double oldvolatility, double quantity, double deviation, double v)
    {
        var a = Math.Log(Math.Pow(oldvolatility, 2));
        var convergenceTolerance = 0.000001;

        var A = a;
        double B;
        if (Math.Pow(quantity, 2) > Math.Pow(deviation, 2) + v)
        {
            B = Math.Log(Math.Pow(quantity, 2) - Math.Pow(deviation, 2) - v);
        }
        else
        {
            var k = 1;
            while (volatilityIterationFunc(a - k * _systemConstant, a, quantity, deviation, v) < 0)
            {
                k += 1;
            }

            B = a - k * _systemConstant;
        }

        var fA = volatilityIterationFunc(A, a, quantity, deviation, v);
        var fB = volatilityIterationFunc(B, a, quantity, deviation, v);
        while (Math.Abs(B - A) > convergenceTolerance)
        {
            var C = A + (A - B) * fA / (fB - fA);
            var fC = volatilityIterationFunc(C, a, quantity, deviation, v);
            if (fC * fB <= 1)
            {
                A = B;
                fA = fB;
            }
            else
            {
                fA /= 2;
            }

            B = C;
            fB = fC;
        }

        return Math.Pow(Math.E, A / 2);
    }

    [Pure]
    private double volatilityIterationFunc(double x, double a, double quantity, double deviation, double v) =>
        ((Math.Pow(Math.E, x) * (Math.Pow(quantity, 2) - Math.Pow(deviation, 2) - v - Math.Pow(Math.E, x))) /
         (2 * Math.Pow(Math.Pow(deviation, 2) + v + Math.Pow(Math.E, x), 2))) -
        ((x - a) / Math.Pow(_systemConstant, 2)); 

    [Pure]
    private double GFunc(double rd) => 1 / Math.Sqrt(1 + 3 * Math.Pow(rd, 2) / Math.Pow(Math.PI, 2));
    
    [Pure]
    private double EFunc(double rtng, double oprtng, double oprd) => 1 / (1 + Math.Exp(-GFunc(oprd) * (rtng - oprtng)));
}