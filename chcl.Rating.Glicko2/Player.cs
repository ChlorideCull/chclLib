﻿using System;

namespace chcl.Rating.Glicko2;

/// <summary>
/// Representing a player in a Glicko-2 system.
/// </summary>
public class Player
{
    /// <summary>
    /// Unique ID of the Player.
    /// </summary>
    public long Id { get; }
    
    /// <summary>
    /// The player's rating. The average is 1500.
    /// </summary>
    public double Rating { get; }
    
    /// <summary>
    /// The player's rating in Glicko-2 scale. The average is 0.
    /// </summary>
    public double RatingRaw => (Rating - 1500) / 173.7178;

    /// <summary>
    /// The standard deviation of the player's rating, as in the interval we thing they fall in.
    /// </summary>
    public double RatingDeviation { get; }
    
    /// <summary>
    /// The standard deviation of the player's rating in Glicko-2 scale, as in the interval we thing they fall in.
    /// </summary>
    public double RatingDeviationRaw => RatingDeviation / 173.7178;
    
    /// <summary>
    /// The volatility of the player's rating.
    /// </summary>
    public double RatingVolatility { get; }

    /// <summary>
    /// Initialize a new Player.
    /// </summary>
    /// <param name="id">Unique ID used to track the player.</param>
    /// <param name="rating">The player's rating. The average and default for an unranked player is 1500.</param>
    /// <param name="ratingDeviation">The standard deviation of the player's rating, default for an unranked player is 350.</param>
    /// <param name="ratingVolatility">The volatility of the player's rating. Default is 0.06, but can and should be tweaked depending on your game.</param>
    public Player(long id, double rating = 1500, double ratingDeviation = 350, double ratingVolatility = 0.06)
    {
        Id = id;
        Rating = rating;
        RatingDeviation = ratingDeviation;
        RatingVolatility = ratingVolatility;
    }
    
    /// <inheritdoc/>
    public override int GetHashCode()
    {
        return HashCode.Combine(Id, Rating, RatingDeviation, RatingVolatility);
    }
}