﻿Implementaion of [Glicko-2](http://www.glicko.net/glicko/glicko2.pdf), validated against the example in the document.

Create a `Glicko2System`, track some players, record some games, and use `ConcludeRatingPeriod()` to calculate the
rating changes.

AOT compatible.

# Version History

## 1.1.0
- Added `Glicko2System.RecordGame(long, long, GameOutcome)` so you don't have to create unnecessary `Player` objects.
`Glicko2System.RecordGame(Player, Player, GameOutcome)` still exists as a convenience function.
- Enabled documentation generation and documented all members.